﻿namespace ICTPRG527_1
{
    partial class AddToLibrary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.publisherTextBox = new System.Windows.Forms.TextBox();
            this.developerTextBox = new System.Windows.Forms.TextBox();
            this.releaseDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.saveButton = new System.Windows.Forms.Button();
            this.genreComboBox = new System.Windows.Forms.ComboBox();
            this.saveLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.imgPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // titleTextBox
            // 
            this.titleTextBox.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.titleTextBox.Location = new System.Drawing.Point(298, 44);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(281, 20);
            this.titleTextBox.TabIndex = 1;
            this.titleTextBox.Tag = "Title";
            this.titleTextBox.Text = "Title";
            this.titleTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.control_MouseClick);
            this.titleTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // publisherTextBox
            // 
            this.publisherTextBox.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.publisherTextBox.Location = new System.Drawing.Point(298, 97);
            this.publisherTextBox.Name = "publisherTextBox";
            this.publisherTextBox.Size = new System.Drawing.Size(281, 20);
            this.publisherTextBox.TabIndex = 2;
            this.publisherTextBox.Tag = "Publisher";
            this.publisherTextBox.Text = "Publisher";
            this.publisherTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.control_MouseClick);
            this.publisherTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // developerTextBox
            // 
            this.developerTextBox.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.developerTextBox.Location = new System.Drawing.Point(298, 123);
            this.developerTextBox.Name = "developerTextBox";
            this.developerTextBox.Size = new System.Drawing.Size(281, 20);
            this.developerTextBox.TabIndex = 3;
            this.developerTextBox.Tag = "Developer";
            this.developerTextBox.Text = "Developer";
            this.developerTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.control_MouseClick);
            this.developerTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // releaseDateTimePicker
            // 
            this.releaseDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.releaseDateTimePicker.Location = new System.Drawing.Point(298, 149);
            this.releaseDateTimePicker.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.releaseDateTimePicker.Name = "releaseDateTimePicker";
            this.releaseDateTimePicker.Size = new System.Drawing.Size(106, 20);
            this.releaseDateTimePicker.TabIndex = 5;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(298, 278);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // genreComboBox
            // 
            this.genreComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.genreComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.genreComboBox.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.genreComboBox.FormattingEnabled = true;
            this.genreComboBox.Items.AddRange(new object[] {
            "Adventure",
            "Puzzle",
            "FPS",
            "Driving",
            "Simulator",
            "Platformer"});
            this.genreComboBox.Location = new System.Drawing.Point(298, 70);
            this.genreComboBox.Name = "genreComboBox";
            this.genreComboBox.Size = new System.Drawing.Size(281, 21);
            this.genreComboBox.TabIndex = 7;
            this.genreComboBox.Tag = "Genre";
            this.genreComboBox.Text = "Genre";
            this.genreComboBox.DropDown += new System.EventHandler(this.genreComboBox_DropDown);
            this.genreComboBox.DropDownClosed += new System.EventHandler(this.genreComboBox_DropDownClosed);
            this.genreComboBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            this.genreComboBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.control_MouseClick);
            // 
            // saveLabel
            // 
            this.saveLabel.AllowDrop = true;
            this.saveLabel.AutoSize = true;
            this.saveLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this.saveLabel.Location = new System.Drawing.Point(379, 283);
            this.saveLabel.Name = "saveLabel";
            this.saveLabel.Size = new System.Drawing.Size(159, 13);
            this.saveLabel.TabIndex = 8;
            this.saveLabel.Text = "The information has been saved";
            this.saveLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.saveLabel.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 1500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // imgPictureBox
            // 
            this.imgPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgPictureBox.Location = new System.Drawing.Point(49, 44);
            this.imgPictureBox.Name = "imgPictureBox";
            this.imgPictureBox.Size = new System.Drawing.Size(207, 258);
            this.imgPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPictureBox.TabIndex = 0;
            this.imgPictureBox.TabStop = false;
            this.imgPictureBox.Click += new System.EventHandler(this.imgPictureBox_Click);
            this.imgPictureBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.artworkPictureBox_DragDrop);
            this.imgPictureBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.artworkPictureBox_DragEnter);
            // 
            // AddToLibrary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 353);
            this.Controls.Add(this.saveLabel);
            this.Controls.Add(this.genreComboBox);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.releaseDateTimePicker);
            this.Controls.Add(this.developerTextBox);
            this.Controls.Add(this.publisherTextBox);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.imgPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AddToLibrary";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddToLibrary";
            ((System.ComponentModel.ISupportInitialize)(this.imgPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgPictureBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.TextBox publisherTextBox;
        private System.Windows.Forms.TextBox developerTextBox;
        private System.Windows.Forms.DateTimePicker releaseDateTimePicker;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.ComboBox genreComboBox;
        private System.Windows.Forms.Label saveLabel;
        private System.Windows.Forms.Timer timer1;
    }
}