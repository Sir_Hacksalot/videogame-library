﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICTPRG527_1
{
    public partial class AddToLibrary : Form
    {
        public AddToLibrary()
        {
            InitializeComponent();
            ((Control)imgPictureBox).AllowDrop = true;
        }

        private void control_MouseClick(object sender, MouseEventArgs e)
        {
            Control ctrl = (Control)sender;
            if(ctrl is TextBox)
            {
                //if user enters the control, preselect all the text
                ((TextBox)ctrl).SelectAll();
            }
            else if(ctrl is ComboBox)
            {
                ((ComboBox)ctrl).SelectAll();
            }

        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            Control ctrl = (Control)sender;
            if (ctrl is TextBox || ctrl is ComboBox)
            {
                //if user types something  then change forecolour to black
                if (ctrl.Text != "")
                {
                    ctrl.ForeColor = SystemColors.ControlText;
                }
                else
                {
                    //textbox has been cleared, throw up the placeholder text       
                    ctrl.Text = ctrl.Tag.ToString();
                    ctrl.ForeColor = SystemColors.ControlDark;
                }
            }
            
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            //create vg object
            VideoGame vgame = new VideoGame();

            if (titleTextBox.Text != "" && titleTextBox.Text != titleTextBox.Tag.ToString())
            {
                vgame.Title = titleTextBox.Text;

                if(genreComboBox.Text != "" && genreComboBox.Text != genreComboBox.Tag.ToString())
                {
                    vgame.Genre = genreComboBox.Text;

                    if(developerTextBox.Text != "" && developerTextBox.Text != developerTextBox.Tag.ToString())
                    {
                        vgame.Developer = developerTextBox.Text;

                        if (publisherTextBox.Text != "" && publisherTextBox.Text != publisherTextBox.Tag.ToString())
                        {
                            vgame.Publisher = publisherTextBox.Text;
                            //release date can't be screwed up
                            vgame.ReleaseDate = releaseDateTimePicker.Value;

                            //attempt to save.....here we go....

                            if (imgPictureBox.Image != null)
                            {
                                try
                                {
                                    imgPictureBox.Image.Tag = vgame.Title;
                                    vgame.Img = imgPictureBox.Image.Tag.ToString();
                                    imgPictureBox.Image.Save("artwork\\" + vgame.Img + ".png", ImageFormat.Png);

                                    //add to the list
                                    Globals.library.VideoLibrary.Add(vgame);

                                    //update combobox on form1???
                                    Globals.libraryChanged = true;

                                    //save the data
                                    FileManager.SaveData();

                                    //give feedback
                                    saveLabel.Visible = true;
                                    timer1.Start();
                                }
                                catch (Exception)
                                {
                                    //bad things happened
                                    MessageBox.Show("An error occured while saving the file.\nSelect an image file to use as the artwork.");
                                    imgPictureBox_Click(sender, e);
                                }
                            }
                            else
                            {
                                //nothing yet. Should be rock solid
                                MessageBox.Show("Select an image file to use as the artwork.");
                                imgPictureBox_Click(sender, e);
                            }
                            

                            
                        }
                        else
                        {
                            MessageBox.Show("Enter a publisher");
                            publisherTextBox.SelectAll();
                            publisherTextBox.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter a developer");
                        developerTextBox.SelectAll();
                        developerTextBox.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Enter a genre");
                    genreComboBox.SelectAll();
                    genreComboBox.Focus();
                }

            }
            else
            {
                MessageBox.Show("Enter a title");
                titleTextBox.SelectAll();
                titleTextBox.Focus();
            }
            
        }


        private void genreComboBox_DropDown(object sender, EventArgs e)
        {
            genreComboBox.ForeColor = SystemColors.ControlText;
        }

        private void genreComboBox_DropDownClosed(object sender, EventArgs e)
        {

            if(genreComboBox.Text != null && genreComboBox.Text != "Genre")
            {
                genreComboBox.ForeColor = SystemColors.ControlText;
            }
            else
            {
                genreComboBox.Text = "Genre";
                genreComboBox.ForeColor = SystemColors.ControlDark;
            }
         
        }

        private void artworkPictureBox_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                imgPictureBox.Image = Image.FromFile(files[0]);
            }
            catch(Exception)
            {
                MessageBox.Show("Incorrect file type.");
            }
            
        }

        private void artworkPictureBox_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void imgPictureBox_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = "Choose an image";
                ofd.Filter = "Image Files (*.bmp;*.jpg;*.jpeg,*.png)|*.BMP;*.JPG;*.JPEG;*.PNG";

                //open a file picker dialog
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    imgPictureBox.Image = new Bitmap(ofd.FileName);
                }
            }
                
        }

        private void timer1_Tick(object sender, EventArgs e)
        {     
            if(saveLabel.Visible == true)
            {
                saveLabel.Visible = false;
                timer1.Stop();
            }
        }
    }
}
