﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICTPRG527_1
{
    [Serializable]
    public class Library
    {
        public Library()
        {
            //nothing yet
        }

        //copy constructor
        public Library(Library library)
        {
            videoLibrary = library.videoLibrary;
        }

        //somewhere to keep all of these games
        private List<VideoGame> videoLibrary = new List<VideoGame>();

        public List<VideoGame> VideoLibrary
        {
            get { return videoLibrary; }
            set { videoLibrary = value; }
        }

        void AddToLibrary(VideoGame game)
        {
            videoLibrary.Add(game);
        }

        void RemoveFromLibrary(int index)
        {
            videoLibrary.RemoveAt(index);
        }
    }
}
