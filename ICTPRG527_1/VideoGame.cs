﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICTPRG527_1
{
    [Serializable]
    public class VideoGame
    {
        private String title;
        private String genre;
        private String publisher;
        private String developer;
        private DateTime releaseDate = new DateTime();
        private String img;


        public VideoGame()
        {
            //hmmm
        }

        public String Title
        {
            get { return title; }
            set { title = value; }
        }
        public String Genre
        {
            get { return genre; }
            set { genre = value; }
        }
        public String Publisher
        {
            get { return publisher; }
            set { publisher = value; }
        }
        public String Developer
        {
            get { return developer; }
            set { developer = value; }
        }
        public DateTime ReleaseDate
        {
            get { return releaseDate; }
            set { releaseDate = value; }
        }
        public String Img
        {
            get { return img; }
            set { img = value; }
        }

        public override String ToString()
        {
            String output = "Title: " + title +
                    '\n' + "Genre: " + genre +
                    '\n' + "Publisher: " + publisher +
                    '\n' + "Developer: " + developer +
                    '\n' + "Release date: " + releaseDate.ToShortDateString();
            return output;
        }

    }
}
