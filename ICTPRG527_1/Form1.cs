﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICTPRG527_1
{
    public partial class Form1 : Form
    {
        static int currentIndex = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void FillComboBox()
        {
            //fill the combobox from the library
            foreach (var item in Globals.library.VideoLibrary)
            {
                comboBox1.Items.Add(item.Title);
            }
        }

        private void addToLibraryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddToLibrary addForm = new AddToLibrary();
            addForm.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get the selected index
            int index = comboBox1.SelectedIndex;

            //show the info
            showInfo(comboBox1.SelectedIndex);
        }

        private void LoadImage(String fileName)
        {
            //dispose of the previous image, if required
            if(pictureBox1.Image != null)
            {
                pictureBox1.Image.Dispose();
            }
            pictureBox1.Image = LoadUnlockedImage(fileName);
        }

        private Bitmap LoadUnlockedImage(String fileName)
        {
            using (Bitmap bm = new Bitmap(fileName))
            {
                //return bitmap with the copy constructor
                return new Bitmap(bm);
            }
        }

        private void showInfo(int index)
        {
            currentIndex = index;

            //check if there are any items in the library
            if (Globals.library.VideoLibrary.Count > 0)
            {
                //load the info into the textbox
                richTextBox1.Text = Globals.library.VideoLibrary[index].ToString();
                //load the image
                //check if it exists first
                String path = @"artwork\" + Globals.library.VideoLibrary[index].Img + ".png";
                if (File.Exists(path))
                {
                    //load the image
                    LoadImage(path);
                }
                else
                {
                    //show default img from resources - image should be embedded
                    pictureBox1.Image = Properties.Resources.noimage;
                }
            }
            else
            {
                //show default img
                pictureBox1.Image = Properties.Resources.noimage;
                //clear the richtextbox
                richTextBox1.Clear();
                comboBox1.Text = "";
            }
        }


        //private void showInfo(int index)
        //{
        //    currentIndex = index;

        //    if (Globals.library.VideoLibrary.Count > 0)
        //    {
        //        //load the info into the textbox
        //        richTextBox1.Text = Globals.library.VideoLibrary[index].ToString();
        //        //load the image
        //        //check if it exists first
        //        String path = @"artwork\" + Globals.library.VideoLibrary[index].Img + ".png";
        //        if (File.Exists(path))
        //        {
        //            //old way that doesn't work
        //            //pictureBox1.Image = Image.FromFile(@"artwork\" + Globals.library.VideoLibrary[index].Img + ".png", true);

        //            //new method which doesn't lock the image
        //            LoadImage(path);
        //        }
        //        else
        //        {
        //            //show default img
        //            //pictureBox1.Image = Image.FromFile(@"artwork\" + "noimage" + ".jpg", true);
        //            //LoadImage(@"artwork\" + "noimage" + ".jpg");
        //            pictureBox1.Image = Properties.Resources.noimage;
        //        }
        //    }
        //    else
        //    {
        //        //show default img
        //        //LoadImage(@"artwork\" + "noimage" + ".jpg");
        //        pictureBox1.Image = Properties.Resources.noimage;
        //        //clear the richtextbox
        //        richTextBox1.Clear();
        //        comboBox1.Text = "";
        //    }

        //}

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            if(Globals.libraryChanged)
            {
                comboBox1.Items.Clear();
                FillComboBox();
                Globals.libraryChanged = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //close the application
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutBox = new AboutBox1();
            aboutBox.Show();
        }

        private void gettingStartedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"help\index.html");
            
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            if (Globals.library.VideoLibrary.Count > 0)
            {
                if (currentIndex < Globals.library.VideoLibrary.Count-1)
                {
                    showInfo(currentIndex + 1);
                }
                else
                {
                    showInfo(0);
                }
            }
            else
            {
                //do nothing
            }
        }

        private void previousButton_Click(object sender, EventArgs e)
        {
            if (Globals.library.VideoLibrary.Count > 0)
            {
                if (currentIndex > 0)
                {
                    showInfo(currentIndex - 1);
                }
                else
                {
                    showInfo(Globals.library.VideoLibrary.Count - 1);
                }
            }
            else
            {
                //do nothing
                showInfo(currentIndex - 1);
            }
                
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (Globals.library.VideoLibrary.Count > 0)
            {
                ////remove image
                File.Delete(@"artwork\" + Globals.library.VideoLibrary[currentIndex].Img + ".png");
                //remove item from library
                Globals.library.VideoLibrary.RemoveAt(currentIndex);
                //remove item from combobox
                comboBox1.Items.RemoveAt(currentIndex);

                //set previous image and info
                previousButton_Click(sender, e);
            }
            else
            {
                //do nothing
            }

            //save the progress, just in case
            FileManager.SaveData();
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //save the progress, just in case
            FileManager.SaveData();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //save the progress
            FileManager.SaveData();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //load the data
            FileManager.LoadData();
            //populate controls
            FillComboBox();
            showInfo(0);
        }
    }
}
