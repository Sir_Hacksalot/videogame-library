﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ICTPRG527_1
{
    class FileManager
    {
        static String fileName = "data.bin";
        static String filePath = System.IO.Directory.GetCurrentDirectory();
        static String fileAndPath = System.IO.Path.Combine(filePath, fileName);


        private FileManager()
        {
            //never gonna make one of these objects, ever
        }

        static public void SaveImage()
        {
            Stream stream = new FileStream(fileAndPath, FileMode.Create, FileAccess.Write, FileShare.None);
            
            stream.Close();
        }

        static public void SaveData()
        {
            //serialize the data
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(fileAndPath, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, Globals.library);
            stream.Close();
        }

        static public void LoadData()
        {
            //create a formatter object
            IFormatter formatter = new BinaryFormatter();
            //check for file first
            try
            {
                if (File.Exists(fileAndPath))
                {
                    using (Stream stream = new FileStream(fileAndPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        //save to Library object
                        Globals.library = (Library)formatter.Deserialize(stream);
                    }
                }
                else
                {
                    //File does not exist
                    System.Windows.Forms.MessageBox.Show("Unable to find file." + '\n' + "Creating an empty library.", 
                        "Warning", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //create file
                    using (System.IO.FileStream fs = System.IO.File.Create(fileAndPath))
                    {
                        //Nothing much more to do here? Stream will close by itself.
                    }
                }
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("Error encountered while attempting to load file.", 
                    "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                throw;
            }
            
        }

    }
}
